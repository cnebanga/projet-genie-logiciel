package fr.ensai.demo.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
//import java.util.Optional;
import java.util.UUID;
import java.util.Date;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.model.Directory;
import fr.ensai.demo.repository.ScanRepository;
//import fr.ensai.demo.model.ComparisonResult;
//import fr.ensai.demo.model.ScanStats;
//import fr.ensai.demo.model.ScanState;
import fr.ensai.demo.model.LocalFS;
import fr.ensai.demo.model.FileInfo;


@Service
public class ScanService {

    @Autowired
    private ScanRepository scanRepository;

    @Autowired
    private LocalFS localFS; // Ensure that LocalFS bean is injected

    public Scan createScan(Directory directory, int maxDepth, int maxFiles, String fileFilter, String typeFilter) {
        // Generate unique ID for the scan
        UUID scanId = UUID.randomUUID();
        // Record the date of the scan
        Date scanDate = new Date();
        // Start the execution time of the scan
        long startTime = System.currentTimeMillis();
        // Create a new Scan object and save it to the database
        Scan scan = new Scan(scanId, directory.getPath(), maxFiles, maxDepth, fileFilter, typeFilter, scanDate, 0, null);
    
        // Scan the directory using LocalFS and set the FileInfoList
        List<FileInfo> fileInfoList = localFS.scanAndGetInfo(directory);
        scan.setFileInfoList(fileInfoList);

        

        // Calculate and record the execution time of the scan
        long executionTime = System.currentTimeMillis() - startTime;
        scan.setExecutionTime(executionTime);
        // Save the scan to the database
        //scanRepository.save(scan);
        scan = scanRepository.save(scan);
    
        return scan;
    }

}
