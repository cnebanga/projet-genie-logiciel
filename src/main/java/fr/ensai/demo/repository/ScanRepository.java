package fr.ensai.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.UUID;

import org.springframework.lang.NonNull;


import fr.ensai.demo.model.Scan;

@Repository
public interface ScanRepository extends CrudRepository<Scan, Long> {
    // Méthode pour sauvegarder un scan dans la base de données
    @Override
    <S extends Scan> S save(@NonNull S entity);

    // Méthode pour rechercher un scan par son ID
    Optional<Scan> findScanById(@NonNull UUID uuid);


    // Méthode pour supprimer un scan de la base de données
    @Override
    void delete(@NonNull Scan entity);

    void deleteById(int scanId);

    <S extends Scan> S saveAndFlush(@NonNull S entity);

    //Scan findById(int scanId);
}