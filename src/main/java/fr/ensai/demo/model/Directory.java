package fr.ensai.demo.model;

/**
 * Represents a directory in a file system.
 * This class encapsulates the path of the directory.
 */

public class Directory {
    private String path;

    // Constructors, getters, and setters
    public Directory(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Directory{" +
                "path='" + path + '\'' +
                '}';
    }
}
