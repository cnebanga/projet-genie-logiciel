package fr.ensai.demo.model;

import jakarta.persistence.Entity;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Represents a scan of a file system.
 */

@Entity
public class Scan {
    private UUID scanId;
    private String directory;
    private int maxFiles;
    private int maxDepth;
    private String fileFilter;
    private String typeFilter;
    private Date scanDate;
    private long executionTime;
    private List<FileInfo> fileInfoList;

    // Constructors, getters, and setters

    public Scan(UUID i, String directory, int maxFiles, int maxDepth, String fileFilter, String typeFilter, Date scanDate, long executionTime, List<FileInfo> fileInfoList) {
        this.scanId = i;
        this.directory = directory;
        this.maxFiles = maxFiles;
        this.maxDepth = maxDepth;
        this.fileFilter = fileFilter;
        this.typeFilter = typeFilter;
        this.scanDate = scanDate;
        this.executionTime = executionTime;
        this.fileInfoList = fileInfoList;
    }


    //public Scan(String path, int maxDepth2, int maxFiles2, String fileFilter2, String typeFilter2) {

//    }


    public Scan() {
		//TODO Auto-generated constructor stub
	}


	public Scan(int i, String directory2, int maxFiles2, int maxDepth2, String fileFilter2, String typeFilter2,
            Date scanDate2, int executionTime2, Object fileInfoList2) {
        //TODO Auto-generated constructor stub
    }


    // Getters and setters
    public UUID getId() {
        return scanId;
    }

    public void setId(UUID scanId) {
        this.scanId = scanId;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public int getMaxFiles() {
        return maxFiles;
    }

    public void setMaxFiles(int maxFiles) {
        this.maxFiles = maxFiles;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public String getFileFilter() {
        return fileFilter;
    }

    public void setFileFilter(String fileFilter) {
        this.fileFilter = fileFilter;
    }

    public String getTypeFilter() {
        return typeFilter;
    }

    public void setTypeFilter(String typeFilter) {
        this.typeFilter = typeFilter;
    }

    public Date getScanDate() {
        return scanDate;
    }

    public void setScanDate(Date scanDate) {
        this.scanDate = scanDate;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
    }

    public List<FileInfo> getFileInfoList() {
        return fileInfoList;
    }

    public void setFileInfoList(List<FileInfo> fileInfoList) {
        this.fileInfoList = fileInfoList;
    }

    @Override
    public String toString() {
        return "Scan{" +
                "id=" + scanId +
                ", maxFiles=" + maxFiles +
                ", maxDepth=" + maxDepth +
                ", fileFilter='" + fileFilter + '\'' +
                ", typeFilter='" + typeFilter + '\'' +
                ", scanDate=" + scanDate +
                ", executionTime=" + executionTime +
                ", filesInfo=" + fileInfoList +
                '}';
    }


    public Scan orElse(Object object) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'orElse'");
    }
}   