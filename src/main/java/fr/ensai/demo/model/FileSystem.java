package fr.ensai.demo.model;

import java.io.File;
import java.util.List;

/**
 * Represents a file system.
 * This interface defines methods for scanning a directory and retrieving information about a file.
 */

public interface FileSystem {

    void scan(Directory directory, List<FileInfo> fileInfoList);

    FileInfo getInfo(File file);
}