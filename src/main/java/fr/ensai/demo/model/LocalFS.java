package fr.ensai.demo.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Represents a local file system implementation of the FileSystem interface.
 * This class provides methods for scanning a local directory and retrieving information about files in the local file system.
 */

public class LocalFS implements FileSystem {

    //perform a scan -- get all files and infos 
    public void scan(Directory directory, List<FileInfo> fileInfoList) {
        File dir = new File(directory.getPath());
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    // Recursive call to scan subdirectories
                    scan(new Directory(file.getAbsolutePath()), fileInfoList);
                } else {
                    // Process file and add FileInfo to the list
                    fileInfoList.add(getInfo(file));
                }
            }
        }
    }

    /**
     * Returns a list of FileInfo objects representing information about files in the specified directory.
     * @param directory The directory to scan.
     * @return List of FileInfo objects representing information about files in the directory.
     */
    public List<FileInfo> scanAndGetInfo(Directory directory) {
        List<FileInfo> fileInfoList = new ArrayList<>();
        scan(directory, fileInfoList);
        return fileInfoList;
    }

    @Override
    public FileInfo getInfo(File file) {
        // Implementation for getting file information in a local file system
        return new FileInfo(
                file.getName(), //get the name of the file 
                new Date(file.lastModified()),  //modificationdate 
                file.length(), //filesize 
                getFileType(file), //filetype
                file.getParent() //containing directory 
        );
    }


    public String getFileType(File file) {
        // Get the file name
        String fileName = file.getName();
        
        // Get the file extension
        String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
        
        // Map file extensions to file types
        switch (fileExtension.toLowerCase()) {
            case "txt":
                return "Text";
            case "pdf":
                return "PDF Document";
            case "doc":
            case "docx":
                return "Microsoft Word Document";
            default:
                return "Unknown";
        }
    }

}
