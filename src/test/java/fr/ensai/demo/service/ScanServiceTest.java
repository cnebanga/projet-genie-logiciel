package fr.ensai.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
//import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.ensai.demo.model.Directory;
import fr.ensai.demo.model.FileInfo;
import fr.ensai.demo.model.LocalFS;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.repository.ScanRepository;


@ExtendWith(MockitoExtension.class)
public class ScanServiceTest {

    @Mock
    private LocalFS localFS;

    @Mock
    private ScanRepository scanRepository;

    @InjectMocks
    private ScanService scanService;

    @Test
    public Scan testCreateScan() {
        System.out.println("Running testCreateScan()...");
        // Create test data
        String directoryPath = "/Users/ibtissama/Desktop/Test_scan";
        int maxDepth = 2;
        int maxFiles = 1;
        String fileFilter = "*.txt";
        String typeFilter = "text/plain";
    
        // Mock Directory object
        Directory directory = new Directory(directoryPath);
    
        // Mock FileInfo list
        List<FileInfo> fileInfoList = new ArrayList<>();
        fileInfoList.add(new FileInfo("file1.txt", new Date(), 100, "text/plain", directoryPath));
        fileInfoList.add(new FileInfo("file2.txt", new Date(), 200, "text/plain", directoryPath));
    
        // Mock the scan ID
        UUID mockScanId = UUID.randomUUID();
    
        // Mock the scan date
        Date mockScanDate = new Date();
    
        // Mock the execution time
        long mockExecutionTime = 500; // in milliseconds
    
        // Mock the expected scan
        Scan expectedScan = new Scan(mockScanId, directoryPath, maxFiles, maxDepth, fileFilter, typeFilter, mockScanDate,
                mockExecutionTime, fileInfoList);
    
        // Stub the scanAndGetInfo method of localFS to return the mock FileInfo list
        when(localFS.scanAndGetInfo(directory)).thenReturn(fileInfoList);
    
        // Stub the save method of scanRepository to return the expected scan
        when(scanRepository.save(any(Scan.class))).thenReturn(expectedScan);
    
        // Call the createScan method
        Scan createdScan = scanService.createScan(directory, maxDepth, maxFiles, fileFilter, typeFilter);
    
        // Verify that scanRepository.save() was called with the correct parameters
        verify(scanRepository, times(1)).save(any(Scan.class));
    
        // Verify that the created scan matches the expected scan
        assertEquals(expectedScan, createdScan);
        System.out.println("Test testCreateScan() completed.");
        
        return createdScan; // Ensure the created scan is returned
    }
    

}