package fr.ensai.demo.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import fr.ensai.demo.model.Scan;

@DataJpaTest
public class ScanRepositoryTest {

    @Autowired
    private ScanRepository scanRepository;

    @Test
    public void testSaveAndFindById() {
        // Créez un nouvel objet Scan
        Scan scan = new Scan(10, "./", 100, 5, "*.txt", "text", new Date(), 50, null);
        // Définissez les propriétés de l'objet scan si nécessaire

        // Enregistrez l'objet scan dans la base de données
        Scan savedScan = scanRepository.save(scan);

        // Récupérez l'objet scan enregistré en utilisant son ID
        Scan retrievedScan = scanRepository.findScanById(savedScan.getId()).orElse(null);

        // Vérifiez si l'objet scan enregistré et l'objet scan récupéré sont les mêmes
        assertEquals(retrievedScan, savedScan);
    }
}



