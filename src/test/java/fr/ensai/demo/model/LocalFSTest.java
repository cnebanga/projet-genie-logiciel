package fr.ensai.demo.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

//import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

//import fr.ensai.demo.model.Directory;
//import fr.ensai.demo.model.FileInfo;
//import fr.ensai.demo.model.LocalFS;

public class LocalFSTest {

    @Test
    public void testScanAndGetInfo() {
        // Mock a directory to scan
        Directory directory = new Directory("/Users/ibtissama/Desktop/Test_scan");

        // Mock FileInfo objects
        FileInfo fileInfo1 = new FileInfo("file1.txt", null, 100, "text/plain", "test_directory_path");
        FileInfo fileInfo2 = new FileInfo("file2.txt", null, 200, "text/plain", "test_directory_path");

        // Mock the list of FileInfo objects returned by the scanAndGetInfo method
        List<FileInfo> expectedFileInfoList = new ArrayList<>();
        expectedFileInfoList.add(fileInfo1);
        expectedFileInfoList.add(fileInfo2);

        // Mock LocalFS object
        LocalFS localFS = mock(LocalFS.class);

        // Stub the scanAndGetInfo method to return the expected list of FileInfo objects
        when(localFS.scanAndGetInfo(directory)).thenReturn(expectedFileInfoList);

        // Call the scanAndGetInfo method
        List<FileInfo> actualFileInfoList = localFS.scanAndGetInfo(directory);

        // Verify that the returned list of FileInfo objects matches the expected list
        assertEquals(expectedFileInfoList, actualFileInfoList);
    }
}
